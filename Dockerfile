FROM ubuntu:16.04

LABEL version="1.0.0"

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y && \
	apt-get install apache2 apache2-utils -y curl && \
	apt-get clean 
# copy index file to /var/www/
COPY index.html /var/www/html

# expose on port 80
EXPOSE 80

# start the apache service as soon as container is created
CMD ["apachectl","-D","FOREGROUND"]
